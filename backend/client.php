<?php
	session_start();
	require_once("login.php");

	$postdata = "";
	$postdata = file_get_contents("php://input");
	$post = json_decode($postdata,true);

	function returnLogged() {
		$ret = Array();
		$ret["user"] = $_SESSION["user"];
		$ret["logged"] = true;
		echo json_encode($ret);
	}

	if(isset($post['register'])&&isset($post['code'])&&isset($post['user'])&&isset($post['pass'])) {
		$log = new logmein();
		$o = $log->register($post['code'],$post['user'],$post['pass']);
		if ($o !== true) {
			$ret = Array();
			$ret["error"] = $o;
			echo json_encode($ret);
			return;
		}
	} else if(isset($post['login'])&&isset($post['user'])&&isset($post['pass'])) {
		$log = new logmein();
		$o = $log->login($post['user'], $post['pass']);
		if(is_array($o) && array_key_exists("aut",$o) && $o["aut"]) {
			echo json_encode($o);
			return;
		} else if ($o === true) {
			returnLogged();
			return;
		}
	} else if(isset($post['logout']) || isset($_POST['logout'])) { 
		$log = new logmein();
		$log->logout();
		return;
	} else if(isset($post['dummy'])  || isset($_POST['dummy'])) {
		return;
	}

	if(isset($_SESSION['logged'])&&isset($_SESSION['userid'])) {
		$log = new logmein();
		$log->increment_visits($_SESSION['userid']);
		returnLogged();
	}
	

?>