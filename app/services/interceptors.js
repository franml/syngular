'use strict';

define(['app'], function (app) {

	app.factory('timeoutHttpIntercept', ['config',function (config) {
		return {
			'request': function(props) {
				props.timeout = config.get('timeout');
				return props;
			}
		};
	}]);

	app.factory('sessionIntercept',['$q','$location','$rootScope',function($q,$location,$rootScope){
		return {
			response: function(r) {
				if (r.data && r.data.error ) {
					switch(r.data.error) {
						case 'E2':
							$rootScope.logout();
							return $q.reject(r);
						case 'E3':
							alert('Bad seriesly user/pwd');
							return $q.reject(r);
					}
				} 
				return r;
			},
			responseError: function(r) {
				return $q.reject(r);
			}
		}
	}]);
	
});

	