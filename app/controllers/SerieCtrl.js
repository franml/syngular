'use strict';

define(['app'], function(app) {

	app.register.controller('SerieCtrl',['$scope','$rootScope','config','$routeParams','spinner','$timeout','Api','data','spin','$window','webStorage',
		function($scope,$rootScope,config,$routeParams,spinner,$timeout,Api,data,spin,$window,webStorage) {

			data.$promise.then(function(){
				$scope.data = data;
				$rootScope.addCrumb(2,$scope.data.name,"/serie/"+$routeParams.idm);		
				spinner.stop(spin);
			});
			$scope.ids = parseInt($routeParams.idm);
			$scope.favorite = false;

			$scope.activeSeason = 1;
			$scope.activeEpisod = { "s":1,"n":null };
			$scope.links = {"streaming":[],"direct_download":[]};
			$scope.languages = {"s":[], "d":[] }; $scope.hosts = {"s":[], "d":[]}; $scope.qualities = {"s":[], "d":[]};

			$scope.isActiveSeason = function(n) { if($scope.activeSeason==n) return 'active'; };
			$scope.isActiveEpisod = function(s,n) { if($scope.activeEpisod.s==s && $scope.activeEpisod.n==n) return 'active'; }
			
			$scope.changeSeason = function(n) { 
				$scope.activeSeason=n;
				$scope.activeEpisod = { "s":n,"n":null };
				$scope.links = {"streaming":[],"direct_download":[]};
			}

			$scope.changeEpisod = function(s,n,id,$event) {
				if(!$scope.spinner || typeof $scope.spinner.el == "undefined") {
					var target = $($event.target).closest('.list-group-item')[0];
					$scope.spinner = spinner.start(target,{length:5,width:2,radius:5,color:'white'},{position:'relative',float:'right',top:'9px',right:'7px'});
					$(target).find('span').hide();
					$scope.activeEpisod.s = s; $scope.activeEpisod.n = n ;
					Api.getLinksSerie({'idm':id},function(r){
						if(!r.error) {
							$scope.links = r;
							$scope.groupFilters();
						} else {
							alert(r.errorMessage);
						}
						$(target).find('span').show();
						spinner.stop($scope.spinner);
						return;
					},function(err){
						spinner.stop($scope.spinner);
					});
				}
			}

			$scope.parseLang = function(s,subs) {
				var s = s.toLowerCase();
				if (typeof subs != 'undefined') var subs=subs.toLowerCase();
				else var subs="";
				if(s.indexOf('original')!=-1) return 'english';
				else if (s.indexOf('ingl')!=-1) return 'english';
				else if (s.indexOf('catal')!=-1) return 'catala';
				else if (s.indexOf('latino')!=-1) return 'latino';
				else if (s.indexOf('castellano')!=-1) return 'castellano';
				else if (s.indexOf('sin audio')!=-1) return 'sinaudio';
				else if (s.indexOf('vo')!=-1 && subs!="" ) return 'vose';
				else return s;
			}

			$scope.groupFilters = function(){
				if ($scope.links.streaming) {
					if ($scope.links.streaming.length < 50) {
						$scope.animate = "animate";
					} else {
						delete $scope.animate;
					}
					$scope.links.streaming.forEach(function(k,v){
						k.lang = $scope.parseLang(k.lang,k.subtitles);
						if($scope.languages.s.indexOf(k.lang) == -1) $scope.languages.s.push(k.lang);
						if($scope.hosts.s.indexOf(k.host) == -1) $scope.hosts.s.push(k.host);
						if($scope.qualities.s.indexOf(k.quality) == -1) $scope.qualities.s.push(k.quality);
					});
				}
				if ($scope.links.direct_download) {
					if ($scope.links.direct_download.length < 50) {
						$scope.animate = "animate";
					} else {
						delete $scope.animate;
					}
					$scope.links.direct_download.forEach(function(k,v){
						k.lang = $scope.parseLang(k.lang,k.subtitles);
						if($scope.languages.d.indexOf(k.lang) == -1)  $scope.languages.d.push(k.lang);
						if($scope.hosts.d.indexOf(k.host) == -1) $scope.hosts.d.push(k.host);
						if($scope.qualities.d.indexOf(k.quality) == -1) $scope.qualities.d.push(k.quality);
					});
				}
			}
			$scope.groupQualities = function(){
				if ($scope.links.streaming)
					$scope.links.streaming.forEach(function(k,v){
						//if (k.features && k.features != "") k.quality = k.quality + ' ' + k.features;
						if($scope.qualities.s.indexOf(k.quality) == -1) $scope.qualities.s.push(k.quality);
					});
				if ($scope.links.direct_download) 
					$scope.links.direct_download.forEach(function(k,v){
						//if (k.features && k.features != "") k.quality = k.quality + ' ' + k.features;
						if($scope.qualities.d.indexOf(k.quality) == -1) $scope.qualities.d.push(k.quality);
					});
				//$scope.qualities.s.sort();
				//$scope.qualities.d.sort();
			}
			$scope.filterProp = function(string,prop) {
				return function(val) {
					if(typeof string == 'undefined ' || string == null) return true;
					return string == val[prop];
				};
			}
			
			$scope.loadLink = function(url,$event) {
				if($rootScope.user.logged) {
					if(!$scope.spinner || typeof $scope.spinner.el == "undefined") {
						var target = $($event.target).closest('.list-group-item')[0];
						$scope.spinner = spinner.start(target,{length:5,width:2,radius:5,color:'blue'},{position:'relative',float:'right',top:'10px',right:'30px'});
						var tab = window.open("about:blank");
						Api.getLink({'link':url},function(data){
							if (data && data.error =="E5") {
								tab.close();
								$rootScope.$broadcast('loginRequired');
							} else if (data && data.url) {
								tab.location = data.url;
							} else {
								tab.close();
							}
							spinner.stop($scope.spinner);
						},function(err){
							tab.close();
							spinner.stop($scope.spinner);							
						});
					}
				} else {
					$rootScope.$broadcast('loginRequired');
				}
			}

			$scope.$on("userChange",function(){
				if($rootScope.user.logged) $scope.checkFavorite();
			});

			$scope.checkFavorite = function() {
				$scope.favorite = $rootScope.user.logged && ($rootScope.user.favs.indexOf($scope.ids) > -1);
			}

			$scope.addFav = function() {
				if(!$scope.busy) {
					$scope.busy = true;
					var params = { 'action':'addfav', 'title':$scope.data.name, 'ids':$scope.ids, 'img': $scope.data.poster.large};
					Api.addFav({},params,function(d){
						if(d.success) $rootScope.user.favs.push($scope.ids);
						$scope.busy = false;
					}, function(err){
						$scope.busy = false;
					});
				}
			}

			$scope.delFav = function() {
				if(!$scope.busy) {
					$scope.busy = true;
					var params = { 'action':'delfav', 'ids':$scope.ids};
					Api.delFav({},params,function(d){
						if(d.success) $rootScope.user.favs.splice($rootScope.user.favs.indexOf($scope.ids),1);
						$scope.busy = false;
					}, function(err){
						$scope.busy = false;
					});
				}
			}
			
			$scope.$watch('[idioma_s, host_s, calidad_s, idioma_d, host_d, calidad_d]', function(newValue,oldValue) {
				['idioma_s', 'host_s', 'calidad_s', 'idioma_d', 'host_d', 'calidad_d'].map(function(k,v){
					if (newValue[v] != oldValue[v]) {
						if (newValue[v]) {
							webStorage.add(k,newValue[v]);
						} else {
							webStorage.remove(k);
						}
					}
				});
			},true);

			$scope.loadFilters = function() {
				['idioma_s', 'host_s', 'calidad_s', 'idioma_d', 'host_d', 'calidad_d'].map(function(k){
					var v = webStorage.get(k);
					if(v) {
						$scope[k] = v;
					}
				});
			}

			var _init = function() {
				$window.scrollTo(0,0);
				$scope.checkFavorite();
				$scope.loadFilters();
			}
			_init();
		}
	]);

});
