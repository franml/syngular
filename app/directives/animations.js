'use strict';

define(['app'], function(app) {

	app.animation('.from-left',function(config){
		if (!config.isMobile.any()) {
			return {
				enter: function(el,done) {
					jQuery(el).css({ position:'absolute', right:'110%' });
					jQuery(el).animate(
						{ right: config.getAbsoluteLeft() },
						config.get('slideTime'),
						'swing',
						function(){ $(el).removeAttr('style'); done(); }
					);
				},
				leave: function(el,done) {
					jQuery(el).css({ position:'absolute', left: config.getAbsoluteLeft() });
					jQuery(el).animate(
						{ left:"110%" },
						config.get('slideTime'),
						'swing',
						function(){ $(el).removeAttr('style'); done(); }
					);
				},
			};
		} else {
			return {
				enter:function(el,done){},
				leave:function(el,done){done();}
			};
		}
	});
	app.animation('.from-right',function(config){
		if (!config.isMobile.any()) {
			return {
				enter: function(el,done) {
					jQuery(el).css({ position:'absolute', left:"110%" });
					jQuery(el).animate(
						{ left: config.getAbsoluteLeft() },
						config.get('slideTime'),
						'swing',
						function(){ $(el).removeAttr('style'); done(); }
					);
				},
				leave: function(el,done) {
					jQuery(el).css({ position:'absolute', right: config.getAbsoluteLeft() });
					jQuery(el).animate(
						{ right:"110%" },
						config.get('slideTime'),
						'swing',
						function(){ $(el).removeAttr('style'); done(); }
					);
				},
			};
		} else {
			return {
				enter:function(el,done){},
				leave:function(el,done){done();}
			};
		}
	});

});