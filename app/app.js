define(['services/routeResolver'],function(){

	var app = angular.module('myApp',['ngRoute','routeResolverServices','Resources','ngAnimate','ui.bootstrap', 'webStorageModule']);

	app.config(['$locationProvider','$routeProvider','routeResolverProvider','$controllerProvider','$compileProvider','$filterProvider','$provide','$httpProvider',
		function($locationProvider,$routeProvider,routeResolverProvider,$controllerProvider,$compileProvider,$filterProvider,$provide,$httpProvider){

			$httpProvider.interceptors.push('timeoutHttpIntercept');
			$httpProvider.interceptors.push('sessionIntercept');
			
			app.register = {
				controller:$controllerProvider.register,
				directive:$compileProvider.register,
				filter:$filterProvider.register,
				factory:$provide.factory,
				service:$provide.service
			};

			//$locationProvider.hashPrefix('!');
			$locationProvider.html5Mode(false);
			var route = routeResolverProvider.route;

			$routeProvider
				.when('/home',route.resolve('Home'))
				.when('/search/:q',route.resolve('Search'))
				.when('/serie/:idm',route.resolve('Serie',{
					spin: ['spinner','$rootScope',function(spinner,$rootScope) {
						$rootScope.spinr = spinner.start(null,{color:'white',overlay:true,opacity:2/4});
						return $rootScope.spinr;
					}],
					data: ['$q','$route','Api','spinner','$rootScope',function($q,$route,Api,spinner,$rootScope) {
						var deferred = $q.defer();
						Api.loadSerie({ 'idm':$route.current.params.idm }
							,function(data) { deferred.resolve(data); }
							,function(error) { 
								spinner.stop($rootScope.spinr);
								deferred.reject(); 
							}
						);
						return deferred.promise;
					}],
					delay: ['$q','$timeout','$cacheFactory','$route',function($q,$timeout,$cacheFactory,$route) {
						var delay = $q.defer();
						if ($cacheFactory.get('$http').get('backend/api.php?action=load&idm='+$route.current.params.idm+'&type=serie')) {
							delay.resolve();
						} else {
							$timeout(delay.resolve,500);
						}
						return delay.promise;
					}]
				}))
				//.when('/serie/:idm', { templateUrl: 'views/Serie.html', controller: SerieCtrl, resolve: SerieCtrl.resolve })
				//.when('/logout',{templateUrl:'views/Dummy.html',controller:'logout'})
				.otherwise({redirectTo:'/home'});
		}
	]);

	app.run(['$rootScope','config','Api','Client','$interval',
		function($rootScope, config, Api, Client, $interval){

			$rootScope.apptitle = config.get('apptitle');
			$rootScope.user = { 
				"logged": false,
				"favs": [],
			 };
			$rootScope.safeApply = function(fn) {
				var phase = this.$root.$$phase;
				if(phase == '$apply' || phase == '$digest') { if(fn && (typeof(fn) === 'function')) fn(); }
				else { this.$apply(fn); }
			};
			$rootScope.$watch("user",function(){ $rootScope.$broadcast('userChange'); },true);

			$rootScope.breadcrumb = [{ 'title':'Inicio','url':'/home','active':'active'}];
			$rootScope.slideClass = "from-left";
			$rootScope.title = 'Inicio';
			$rootScope.addCrumb = function(level,title,url) {
				$rootScope.title = title;
				var from, to = level;
				$rootScope.breadcrumb.forEach(function(k,v){ if(k.active=="active") from = v; });
				$rootScope.breadcrumb[from].active = null;
				$rootScope.breadcrumb[level] = {"title":title,"url":url,"active":"active"};
				if (from == to) return;
				$rootScope.slideClass = from<to?"from-right":"from-left";
				$('.from-right,.from-left').removeClass('from-right').removeClass('from-left').addClass($rootScope.slideClass);
			}
			$rootScope.login = function(user) {
				$rootScope.user.logged = true;
				$rootScope.user.email = user;
				$rootScope.user.name = user;
				$rootScope.user.favs = [];
				Api.userFavs({},function(r){
					if(r && r.length>0) {
						angular.forEach(r,function(k,v){$rootScope.user.favs.push(parseInt(k.ids));});
					}
				});
			}
			$rootScope.logout = function() {
				Client.logout({},{'logout':'logout'},function(){
					$rootScope.user = { logged: false };
				});
				if(angular.isDefined($rootScope.logincheck)) $interval.cancel($rootScope.logincheck);
			}
		}
	]);

	return app;

});