<?php

	function select($query){
		$conn = new mysqli(DBHOST,DBUSER,DBPASS,DBNAME);
		$r = $conn->query($query);
		$arr = Array();
		if($r) {
			while($row = $r->fetch_array(MYSQL_ASSOC)) $arr[] = $row;
			$r->close();
		}
		$conn->close();
		return $arr;
	}

	function insert($query){
		$conn = mysqli_connect(DBHOST,DBUSER,DBPASS,DBNAME);
		$r = mysqli_query($conn,$query);
		mysqli_close($conn);
		return $r;
	}

	function getat() {
		$url = BASEURL."/auth_token/?id_api=".IDAPI."&secret=".SECRET;
		$at = json_decode(geturl($url,1),TRUE);
		$_SESSION['at'] = $at["auth_token"];
		$_SESSION['atexp'] = $at["auth_expires_date"];
		return $at;
	}

	function postut($at) {
		$url = BASEURL."/user/user_login";
		if(!$_SESSION['linked']) { 
			$parms = "auth_token=".$at."&redirect_url=xxx&username=".$_SESSION['user']."&password=".$_SESSION['pass']."&remember=0"; 
		} else { 
			$parms = "auth_token=".$at."&redirect_url=xxx&username=".SLYUSER."&password=".SLYPASS."&remember=0"; 
		}
		$ut = getredir($url."?".$parms,1);
		if ($ut["success"]) {
			$s2 = preg_split('<\?\s*?>',$ut["data"]);
			$s3 = preg_split('<&\s*?>',$s2[1]);
			$s4 = preg_split('<=\s*?>',$s3[0]);
			$s5 = preg_split('<=\s*?>',$s3[1]);
			$_SESSION['ut'] = $s4[1];
			$_SESSION['utexp'] = $s5[1];
		}
		return $ut["success"];
	}

	function expired_at() { return !( isset($_SESSION['at']) && ($_SESSION['atexp']>time()) ) ; }
	function expired_ut() { return !( isset($_SESSION['ut']) && ($_SESSION['utexp']>time()) ) ; }

	function getut($at,$link) {
		$redir = "http://".$_SERVER["HTTP_HOST"].$_SERVER["SCRIPT_NAME"];
		$url = BASEURL."/user/user_login?auth_token=".$at."&redirect_url=".$redir;
		
		if(isset($link))	$url .= "?link=".$link;
		else 				$url .= "?action=getut";
		header('Location: '.$url);
	}

	function geturl ($url,$tries) {
		$i = 0;
		while($i<$tries) {
			$c = curl_init();
			curl_setopt($c, CURLOPT_URL, $url);
			curl_setopt($c, CURLOPT_HEADER, false);
			curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($c, CURLOPT_FOLLOWLOCATION, false);
			//curl_setopt($c, CURLINFO_HEADER_OUT, true);
			//$log = fopen("httplog.log","w");
			//curl_setopt($c, CURLOPT_VERBOSE, true);
	 		//curl_setopt($c, CURLOPT_STDERR, $log);
			//fclose($log);
			$r = curl_exec($c);
			//$info = curl_getinfo($c);
			//return $info;			
			curl_close($c);
			if(strlen($r)>2) return $r;
			$i++;
		}
		return "E1";
	}
	
	function posturl ($url,$parms) {
  		$c = curl_init($url);
 		curl_setopt($c, CURLOPT_POST, true);
 		curl_setopt($c, CURLOPT_POSTFIELDS, $parms);
 		curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
 		$r = curl_exec($c);
		curl_close($c);
		return $r;
 		if(strlen($r)>2)	return $r;
 		else 				return "E1";
	}

	function getredir($url,$tries) {
		$i = 0;
		$ret = Array();
		while($i<$tries) {
			$c = curl_init();
			curl_setopt($c, CURLOPT_URL, $url);
			curl_setopt($c, CURLOPT_HEADER, true);
			curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($c, CURLOPT_FOLLOWLOCATION, false);
			$r = curl_exec($c);
			curl_close($c);
			if(preg_match('#Location: (.*)#', $r, $a)) {
				$ret["success"] = true;
				$ret["data"] = trim($a[1]);
				return $ret;
			}
			$i++;
		}
		$ret["success"] = false;
		$ret["data"] = $r;
		return $ret;
	}
	function getall($url) {
		$c = curl_init();
		curl_setopt($c, CURLOPT_URL, $url);
		curl_setopt($c, CURLOPT_HEADER, true);
		curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($c, CURLOPT_FOLLOWLOCATION, false);
		$r = curl_exec($c);
		curl_close($c);
		return $r;
	}



?>