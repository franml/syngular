<?php

	// 'E2' -> Not logged
	// 'E3' -> Bad Sly Pwd / UT
	// 'E4' -> Bad inv code
	// 'E5' -> Not logged + login required

	session_start();
	require_once("const.php");
	require_once("common.php");

	function searchserie($at,$str) {
		$url = BASEURL."/search?auth_token=".$at."&q=".$str."&filter=1&limit=50";
		return geturl($url,1);
	}

	function loadserie($at,$idm) {
		$url = BASEURL."/media/full_info?auth_token=".$at."&idm=".$idm."&mediaType=1";
		return geturl($url,1);
	}

	function getlinksserie($at,$idm) {
		$url = BASEURL."/media/episode/links?auth_token=".$at."&idm=".$idm."&mediaType=5";
		return geturl($url,1);
	}

	function getlink($at,$ut,$link) {
		$url = $link."?auth_token=".$at."&user_token=".$ut;
		return getredir($url,1);
	}

	function generalfavs() {
		$sql = "SELECT * FROM `favshows` WHERE `general`=1 ORDER BY ID";
		return select($sql);
	}

	function userfavs() {
		$sql = "SELECT `ids` FROM `userfavs` WHERE `user`=".$_SESSION['userid'];
		$arr = select($sql);
		$ids = "";
		foreach ($arr as $k=>$v) $ids .= $v["ids"].",";
		$ids = substr($ids,0,-1);
		$sql = "SELECT * FROM `favshows` WHERE `ids` IN (".$ids.")";
		return select($sql);
	}

	function addfav($title,$ids,$img) {
		$sql = "SELECT `title` FROM `favshows` WHERE `ids`=".$ids;
		$arr = select($sql);
		if(count($arr)==0) {
			$sql = "INSERT INTO `favshows` (`title`,`ids`,`img`,`general`) VALUES ('".$title."','".$ids."','".$img."',0)";
			$r = insert($sql);
			if(!$r) return false;
		}
		$sql = "INSERT INTO `userfavs` (`user`,`ids`) VALUES (".$_SESSION["userid"].",'".$ids."')";
		return insert($sql);
	}
	function deletefav($ids) {
		$sql = "DELETE FROM `userfavs` WHERE `user`=".$_SESSION['userid']." AND `ids`=".$ids;
		return insert($sql);
	}

	if ($_SERVER["REQUEST_METHOD"] == "GET") {

		$op = $_GET['action'];
		switch ($op) {
			case 'search':
				switch($_GET['type']) {
					case 'serie':
						if(expired_at()) getat();
						$r = searchserie($_SESSION['at'],urlencode($_GET['str']));
						echo $r;
						break;
					default:
						break;
				}
				break;
			case 'load':
				switch($_GET['type']) {
					case 'serie':
						if(expired_at()) getat();
						$r = loadserie($_SESSION['at'],$_GET['idm']);
						echo $r;
						break;
					default:
						break;
				}
				break;
			case 'getlinks':
				switch($_GET['type']) {
					case 'serie':
						if(expired_at()) getat();
						$r = getlinksserie($_SESSION['at'],$_GET['idm']);
						echo $r;
						break;
					default:
						break;
				};
				break;
			case 'getlink':
				if(isset($_SESSION['logged'])) {
					$r = Array();
					if(expired_at()) getat();
					if(expired_ut()) {
						$ut = postut($_SESSION['at'],$_GET['link']);
						if (!$ut) { 
							$r['error'] = "E3";
							echo json_encode($r);
							return;
						}
					}
					$ret = getlink($_SESSION['at'],$_SESSION['ut'],$_GET['link']);
					if($ret["success"]) $r["url"] = $ret["data"];
					echo json_encode($r);
				} else {
					$r = Array();
					$r["error"] = "E5";
					echo json_encode($r);
				}
				break;
			case 'generalfavs':
				echo json_encode(generalfavs());
				break;
			case 'userfavs':
				if(isset($_SESSION['logged'])) echo json_encode(userfavs());
				else {
					$r = Array();
					$r['error'] = "E2";
					echo json_encode($r);
				}
				break;
			default: 
				break;
		}
		return;

	} elseif ($_SERVER["REQUEST_METHOD"] == "POST") {

		$postdata = file_get_contents("php://input");
		$post = json_decode($postdata,true);
		$op = $post['action'];
		switch ($op) {
			case 'addfav':
				$r = Array();
				if(isset($_SESSION['logged'])) {
					$r['success'] = false;
					if(isset($post['title']) && isset($post['ids']) && isset($post['img'])) {
						$r['success'] = addfav($post['title'],$post['ids'],$post['img']);
					}
				} else {
					$r['error'] = 'E2';
				}
				echo json_encode($r);
				break;
			case 'delfav':
				$r = Array();
				if(isset($_SESSION['logged'])) {
					$r['success'] = false;
					if(isset($post['ids'])) $r['success'] = deletefav($post['ids']);
				} else {
					$r['error'] = 'E2';
				}
				echo json_encode($r);
				break;
			default: 
				break;
		}
		return;

	}

?>