'use strict';

define(['app'], function (app) {

	app.factory('datatable',function($timeout,config) {
		return {
			initTable: function(table,idlecols) {
				$(function() {
					$.extend($.tablesorter.themes.bootstrap, {
						table      : 'table table-striped table-hover table-bordered',
						caption    : 'caption',
						header     : 'bootstrap-header',
						sortNone   : 'bootstrap-icon-unsorted',
						sortAsc    : 'icon-chevron-up glyphicon glyphicon-chevron-up',    
						sortDesc   : 'icon-chevron-down glyphicon glyphicon-chevron-down',
					});
				});

				$timeout(function(){
					var sorterOptions = {
						theme:"bootstrap",
						widthFixed:true,
						headerTemplate:'{content} {icon}',
						showProcessing: false,
						sortReset:true,
						sortRestart:true,
						headers: {},
						widgets: ['uitheme','zebra'],
						widgetOptions: { 
							columns: ['primary'], 
							columns_thead: true, 
							columns_tfoot: false,
							zebra: ["even", "odd"],
						}
					};
					if(idlecols) idlecols.forEach(function(e){ sorterOptions.headers[e] = { sorter:false };})
					var pagerOptions = {
						container: $(".pagination"),
						ajaxUrl: null,
						customAjaxUrl: function(table, url) { return url; },
						ajaxProcessing: function(ajax){ if (ajax && ajax.hasOwnProperty('data')) { return [ ajax.total_rows, ajax.data ]; } },
						output: 'Mostrando del <strong>{startRow}</strong> al <strong>{endRow}</strong> de <strong>{totalRows}</strong> resultados',
						updateArrows: true,
						page: 0,
						size: config.get("TABLE_ROWS"),
						fixedHeight: true,
						removeRows: false,
						cssFirst: '.first',
						cssLast: '.last',
						cssGoto: '.gotoPage',
						cssDisabled: 'disabled',
					};
					table.tablesorter(sorterOptions).tablesorterPager(pagerOptions).show();
				},1);
			},
			getPic: function(ref) {
				if(ref) return ref.replace(/\//g,"")+".jpg";
			}
		}
	})


});