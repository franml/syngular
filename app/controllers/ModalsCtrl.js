'use strict';

define(['app'], function(app) {

	app.controller('LoginCtrl',['$rootScope','$scope','Client','$modalInstance','$timeout','Api','$window',
		function($rootScope,$scope,Client,$modalInstance,$timeout,Api,$window){

			$scope.data = {
				user: "",
				pass: "",
				wrong: false,
				error: false,
			}

			$scope.login = function() {
				var params = { login:'login', user: $scope.data.user, pass: $scope.data.pass };
				Client.login({},params,function(data){
					if (data && data.user && data.logged){
						var params2 = { dummy: 'dummy' };
						Client.dummy({},params2,function(){ 
							$window.location.reload(); 
						});
					} else if (data && data.aut && data.url) {
						window.open(data.url,"_blank");
					} else {
						$scope.data.wrong = true;
						$timeout(function(){$scope.data.wrong=false;},10000)
					}
				},function(err){
					$scope.data.error = true;
					$timeout(function(){$scope.data.error=false;},10000)
				});
			}

			$scope.cancel = function() { $modalInstance.dismiss('cancel'); }

	}]);

	app.controller('RegisterCtrl',['$rootScope','$scope','Client','$modalInstance','$timeout','$http',
		function($rootScope,$scope,Client,$modalInstance,$timeout,$http){

			$scope.data = {
				code: "",
				user: "",
				pass: "",
				wrong: false,
				exists: false,
				error: false,
			}

			$scope.invitation = function() {
				var params = { register:'register', code:$scope.data.code, user: $scope.data.user, pass: $scope.data.pass };
				Client.register({},params,function(data){
					if (data && data.user && data.logged){
						$rootScope.login(data.user);
						$modalInstance.close();
					} else if (data && data.error == "E4") {
						$scope.data.wrong = true;
						$timeout(function(){$scope.data.wrong=false;},10000)
					} else if (data && data.error == "E3") {
						$scope.data.exists = true;
						$timeout(function(){$scope.data.exists=false;},10000)
					} else {
						$scope.data.error = true;
						$timeout(function(){$scope.data.error=false;},10000)
					}
				},function(err){
					$scope.data.error = true;
					$timeout(function(){$scope.data.error=false;},10000)
				});

			}
			$scope.cancel = function() { $modalInstance.dismiss('cancel'); }

	}]);

});