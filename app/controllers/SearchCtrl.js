'use strict';

define(['app'], function(app) {

	app.register.controller('SearchCtrl',['$scope','$rootScope','$routeParams','$location','datatable','Api','spinner',
		function($scope,$rootScope,$routeParams,$location,datatable,Api,spinner) {

			$scope.results = [];
			$scope.empty = true;

			var search = function() {
				$scope.spinner = spinner.start(null,{overlay:true});
				Api.searchSeries({'str':$routeParams.q},function(r){
					if(r.error) $scope.results = [];
					else {
						$scope.results = r.response.results;
						$scope.empty  = $scope.results.length == 0;
						datatable.initTable($('.datatable'),[0]);
					}
					spinner.stop($scope.spinner);
				});
			};

			$scope.load = function(id,name) {
				$location.path('/serie/'+id); 
			}

			$scope.isEmpty = function() { if ($scope.empty === true) return 'disabled'; }

			var _init = function(){
				//alert($routeParams.q);
				$rootScope.addCrumb(1,"Búsqueda: "+$routeParams.q,"/search/"+$routeParams.q);
				search();
				$('.datatable').hide();
			};
			_init();

	}]);
});