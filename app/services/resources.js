'use strict';

define([],function(){

	var Resources = angular.module('Resources',['ngResource'])

	Resources.factory('Client', ['$resource','$http',
		function($resource,$http){
			return $resource('backend/client.php',{}, {
				'check': {
					method: 'GET',
				},
				'login': {
					method: 'POST',
					//params: {login:'@login',user:'@user',pass:'@pass'},
					headers : {'Content-Type': 'application/x-www-form-urlencoded'}
				},
				'logout': {
					method: 'POST',
					//params: {logout:'@logout'},
					headers : {'Content-Type': 'application/x-www-form-urlencoded'}
				},			
				'query': {
					method: 'GET',
					params: {action:'@action'},
					isArray: true
				},
				'register': {
					method: 'POST',
					//params: {register:'@register',code:'@code',user:'@user',pass:'@pass'},
					headers : {'Content-Type': 'application/x-www-form-urlencoded'},
					isArray: false,
					/*transformResponse: [function(data, headersGetter) {
							return { response: data };
						}].concat($http.defaults.transformResponse)*/
				},
				'dummy': {
					method: 'POST',
					//params: {dummy:'@dummy'},
					headers : {'Content-Type': 'application/x-www-form-urlencoded'}
				}
			});
		}
	]);

	Resources.factory('Api', ['$resource',
		function($resource){
			return $resource('backend/api.php',{}, {
				'generalFavs': {
					method: 'GET',
					params: {'action':'generalfavs'},
					isArray: true,
					cache: true
				},
				'userFavs': {
					method: 'GET',
					params: {'action':'userfavs'},
					isArray: true,
				},
				'addFav': {
					method: 'POST',
					//params: {action:'@action',title:'@title',ids:'@ids',img:'@img'},
					headers : {'Content-Type': 'application/x-www-form-urlencoded'},
					isArray: false,
				},
				'delFav': {
					method: 'POST',
					//params: {action:'@action',ids:'@ids'},
					headers : {'Content-Type': 'application/x-www-form-urlencoded'},
					isArray: false,
				},
				'searchSeries': {
					method: 'GET',
					params: {'action':'search','type':'serie','str':'@str'},
					isArray: false,
					cache: true
				},
				'loadSerie': {
					method: 'GET',
					params: {'action':'load','type':'serie','idm':'@idm'},
					isArray: false,
					cache: true
				},
				'getLinksSerie': {
					method: 'GET',
					params: {'action':'getlinks','type':'serie','idm':'@idm'},
					isArray: false,
					cache: true
				},
				'getLink': {
					method: 'GET',
					params: { 'action':'getlink','link':'@link'},
					isArray: false,
					cache: true
				},
			});
		}
	]);

});