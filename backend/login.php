<?php
require_once("const.php");
require_once("common.php");

class logmein {

	var $conn = '';

	function dbconnect() {
		$connections = mysql_connect(DBHOST,DBUSER,DBPASS) or die ('Unable to connect to the database');
		mysql_select_db(DBNAME) or die('Unable to select database');
		return $connections;
	}

	function login($user,$pass) {
		$result = $this->qry("SELECT `userid`,`email`,`linked`,`pass` FROM `logon` WHERE `email`='?';", $user);
		$row=mysql_fetch_assoc($result);
	
		if($row && $row['email']!="") {

			if($row['pass'] == $pass) {
				unset($_SESSION['ut']);
				unset($_SESSION['utexp']);
				$_SESSION['logged'] = true;
				$_SESSION['user'] = $row["email"];
				$_SESSION['userid'] = $row["userid"];
				$_SESSION['linked'] = $row["linked"];
				$_SESSION['pass'] = $row["pass"];
				$this->increment_logins($_SESSION['userid']);
				mysql_close($this->conn);
				return true;
			} else {
				return false;
			}

		} elseif(expired_ut())  {

			if(expired_at()) getat();
			$url = BASEURL."/user/user_login";
			$parms1 = "auth_token=".$_SESSION['at']."&redirect_url=http://syngular.atspace.eu/backend/catch.php";
			$parms2 = "&username=".$user."&password=".$pass."&remember=0";
			$r = getall($url."?".$parms1.$parms2);

			if(preg_match('#Location: (.*)#', $r, $a)) { // USER+PWD IS VALID AND SYNGULAR IS AUTHORIZED
				$id = $this->new_user($user,$pass);
				if($id != false) { $_SESSION['userid'] = $id; }
				$this->setSession($user,$pass);
				$this->parseUt(trim($a[1]));
				return true;
			} else if (strpos($r,"permissions") !== false) { // USER+PWD IS VALID AND SYNGULAR IS UNAUTHORIZED
				$_SESSION['user'] = $user;
				$_SESSION['linked'] = false;
				$_SESSION['pass'] = $pass;
				$ret = Array();
				$ret["aut"] = true;
				$ret["url"] = $url."?".$parms1.$parms2;
				return $ret;
			} else { // USER+PWD IS NOT VALID
				return false;
			}			
			

		} else if($user==$_SESSION['user'] && $pass==$_SESSION['pass']) {

			$id = $this->new_user($user,$pass);
			if($id != false) { $_SESSION['userid'] = $id; }
			mysql_close($this->conn);
			$this->setSession($user,$pass);
			return true;

		} else {
			
			unset($_SESSION);

		}
		mysql_close($this->conn);
		return false;
	}

	function register($code,$user,$pass) {
		if ($code != INV_CODE) return "E4";
		$result = $this->qry("SELECT `email` FROM `logon` WHERE `email`='?';", $user);
		$row=mysql_fetch_assoc($result);	
		if($row && $row['email']!="") return "E3";
		$result = $this->qry("INSERT INTO `logon` (`email`,`linked`,`pass`) VALUES ('?','1','?')",$user,$pass);
		if($result) return $this->login($user,$pass);
		return;
	}

	function parseUt($ut) {
		$s2 = preg_split('<\?\s*?>',$ut);
		$s3 = preg_split('<&\s*?>',$s2[1]);
		$s4 = preg_split('<=\s*?>',$s3[0]);
		$s5 = preg_split('<=\s*?>',$s3[1]);
		$_SESSION['ut'] = $s4[1];
		$_SESSION['utexp'] = $s5[1];
	}

	function setSession($user,$pass) {
		$_SESSION['logged'] = true;
		$_SESSION['user'] = $user;
		$_SESSION['linked'] = false;
		$_SESSION['pass'] = $pass;
	}

	function increment_logins($user) {
		$result = $this->qry("UPDATE `logon` SET totallogins=totallogins+1 WHERE `userid`='?'", $user);
		return;
	}

	function increment_visits($user) {
		$result = $this->qry("UPDATE `logon` SET totalvisits=totalvisits+1 WHERE `userid`='?'", $user);
		return;
	}

	function new_user($user,$pass) {
		$result = $this->qry("INSERT INTO `logon` (`email`,`pass`,`totallogins`,`totalvisits`) VALUES ('?','?','1','0')",$user,$pass);
		$result = $this->qry("SELECT `userid` FROM `logon` WHERE `email`='?' AND `pass` = '?'", $user,$pass);
		$row=mysql_fetch_assoc($result);
		if($row) return $row['userid'];
		return;
	}

	function insertError($user,$pass,$url,$ut) {
		$result = $this->qry("INSERT INTO `test` (`user`,`pass`,`url`,`msg`) VALUES ('?','?','?','?')",$user,$pass,$url,$ut);
		return;
	}

	function qry($query) {
		$this->conn = $this->dbconnect();
		$args = func_get_args();
		$query = array_shift($args);
		$query = str_replace("?","%s",$query);
		$args = array_map('mysql_real_escape_string',$args);
		array_unshift($args,$query);
		$query = call_user_func_array('sprintf',$args);
		$result = mysql_query($query) or die(mysql_error());
		if($result) return $result;
		return "qry Escape Error";
	}

	function logout() { 
		session_unset();
		session_destroy();
		return;
	}

}
?>