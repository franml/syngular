'use strict';

define(['app'], function(app) {

	app.controller('HeaderCtrl',['$rootScope','$scope','Client','$location','$modal','config','$interval',
		function($rootScope,$scope,Client,$location,$modal,config,$interval){

			$scope.error = "", $scope.query = "";

			$scope.openLoginModal = function(){
				var options = {
					templateUrl: 'views/modal/Login.html',
					controller: 'LoginCtrl',
					resolve: {}
				}
				var modal = $modal.open(options);
				modal.result.then(function(data){}, function(){});
			}

			$scope.openRegisterModal = function(){
				var options = {
					templateUrl: 'views/modal/Register.html',
					controller: 'RegisterCtrl',
					resolve: {}
				}
				var modal = $modal.open(options);
				modal.result.then(function(data){}, function(){});
			}
			
			$scope.logout = function() {
				$rootScope.logout();
			}
			$scope.search = function() {
				$location.path('/search/'+$scope.query);
			}
			$scope.notEmpty = function(o) {
				return o && o.title != undefined;
			}

			$scope.$on('loginRequired',function(){
				$rootScope.user.logged = false;
				$scope.openLoginModal();
			})

			var _init = function(){
				Client.check(null,function(data){
					if (data && data.user) {
						$rootScope.login(data.user);
						$rootScope.logincheck = $interval(function(){Client.check();},config.get('sessionTime'));
					}
				});
			};
			_init();

	}]);

});