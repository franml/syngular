'use strict';

define(['app'], function(app) {

	app.register.controller('HomeCtrl',['$scope','$rootScope','config','$http','$location','Api','spinner','$window','$timeout',
		function($scope,$rootScope,config,$http,$location,Api,spinner,$window,$timeout) {

			$scope.data = [];
			$scope.error = "";
			$scope.panels = {'general':false,'user':false};
			$scope.generalFavs = []; $scope.userFavs = [];
			$scope.loadingUser = $scope.loadingGeneral = true;

			$scope.toggle = function(panel) { $scope.panels[panel] = !$scope.panels[panel] }
			$scope.getClass = function(panel) { if($scope.panels[panel]) return 'small'; else return 'full'; }

			$scope.$on("userChange",function(){
				if($rootScope.user.logged) {
					$scope.loadUserFavs();
				} else {
					$scope.userFavs = [];
				}
			});

			$scope.loadGeneralFavs = function() {
				Api.generalFavs({},function(r){
					$scope.generalFavs = r;
					$scope.loadingGeneral = false;
				},function(e){
					$scope.loadingGeneral = false;
				});
			}

			$scope.loadUserFavs = function() {
				Api.userFavs({},function(r){
					$scope.userFavs = r;
					$scope.loadingUser = false;
				},function(e){
					$scope.loadingUser = false;
				});
			}

			$scope.loadSerie = function(id,name) {
				$location.path('/serie/'+id); 
			}

			var _init = function() {
				$rootScope.addCrumb(0,'Inicio','/home');
				$scope.loadGeneralFavs();
				if($rootScope.user.logged) $scope.loadUserFavs();
				$window.scrollTo(0,0);
			}
			_init();
		}
	]);
});